package se.gimerstedt;

import static java.util.UUID.randomUUID;

public class TestFlyer implements DeltaFlyable {
    private final Long id;
    private final String rand;

    public TestFlyer(Long id) {
        this.id = id;
        rand = randomUUID().toString();
    }

    public Long getId() {
        return id;
    }

    public String getRand() {
        return rand;
    }
}
