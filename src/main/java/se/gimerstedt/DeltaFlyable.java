package se.gimerstedt;

public interface DeltaFlyable {
    /*
     * unique identifier for the item
     */
    Long getId();
}
